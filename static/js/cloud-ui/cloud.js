/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Encapsulate the communications with the cloud server
 */



function Cloud() {
	var self = this;

	/* -----------------------------------------------------------------------
	   Retrieve and interpret cloud's server info 
	   ----------------------------------------------------------------------- */
	this.setup = function() {
		var ret = $.Deferred();

		$.ajax({
			url         : '/info.json',
			type        : 'GET',
			contentType : 'application/json',
		})
		.done(function(data) {
			self.url = data['cloud-url'];
			ret.resolve();
		})
		.fail(function(jqXHR, status, error) {
			ret.reject(jqXHR, status, error);
		});

		return ret.promise();
	}	

	/* -----------------------------------------------------------------------
	   Get the list of transcription tables
	   ----------------------------------------------------------------------- */
	this.get_transcript_table_list = function() {
		var ret = $.Deferred();

		$.ajax({
			url         : self.url + '/pfkit/transcript-table-list.json',
			type        : 'GET',
			contentType : 'application/json',
		})
		.done(function(data) {
			ret.resolve(data['transcript-table-list']);
		})
		.fail(function(jqXHR, status, error) {
			ret.reject(jqXHR, status, error);
		});

		return ret.promise();	
	}

	/* -----------------------------------------------------------------------
	   run a task and get its result
	   ----------------------------------------------------------------------- */
	this.run = function(function_url, input_data) {
		var base_url = self.url + function_url;
		var task_url = base_url + '/tasks/';

		// Submit data
		function submit_task() {
			return $.ajax({
				url         : base_url,
				type        : 'POST',
				data        : JSON.stringify(input_data),
				contentType : 'application/json',
				dataType    : 'json',
			});
		}

		// Fetch data
		function fetch_result(submit_task_result) {
			var task_id = submit_task_result['task-id'];
			var ret = $.Deferred();

			// Poll the cloud server until we get an answer
			// See http://techoctave.com/c7/posts/60-simple-long-polling-example-with-javascript-and-jquery/
			var completed = false;
			(function poll() {
				$.ajax({
						url         : task_url + task_id,
						type        : 'GET',
						dataType    : 'json',
						timeout     : 10000,
						success     : function(data, status, jqXHR) {
						              	status_code = jqXHR.statusCode().status;
						              	// Result is not available : poll
						              	if (status_code == 204) {
						              		poll();
						              	}
						                // Result is available : done
						              	else if (status_code == 200) {
						              		completed = true;
						              		ret.resolve(task_id, data);
						            		}
						              },
						error       : function(jqXHR, status, error) {
						              	ret.reject(jqXHR, status, error);
						              }
				});
			})();

			return ret.promise();	
		}

		// Delete task
		function delete_task(task_id, result_data) {
			var ret = $.Deferred();

			$.ajax({
				url         : task_url + task_id,
				type        : 'DELETE',
				dataType    : 'text',
			})
			.done(function(data) {
				ret.resolve(result_data);
			})
			.fail(function(jqXHR, status, error) {
				ret.reject(jqXHR, status, error);
			});

			return ret.promise();	
		}

		// Start the submit/fetch/delete sequence
		var ret = $.Deferred();

		submit_task()
			.then(fetch_result)
			.then(delete_task)
			.then(function(data) {
				ret.resolve(data);
			});

		return ret.promise();
	}
}
