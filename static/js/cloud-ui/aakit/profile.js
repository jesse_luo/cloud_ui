/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Web Client for aakit/profile
 */



$(document).ready(function() {
  // Display & UI
	var submit_button = $("#submit-button");
	var result_area = $("#result-area");

	function set_submit_button_unactive() {
		submit_button.addClass("disabled");
		submit_button.html("Processing...");
	}

	function set_submit_button_active() {
		submit_button.removeClass("disabled");
		submit_button.html("Submit");
	}

	function display_result(result) {
		// Build new result area content
		var ul = jQuery('<ul class="no-bullet"/>');

		item_count = result.length;	
		for(var i = 0; i < item_count; ++i) {
			var out = '';
			var item = result[i];
			var j = 0;
			for (var aacid in item) {
				if (item.hasOwnProperty(aacid)) {
					if (j > 0)
						out += ' ';
					out += aacid + ':' + item[aacid];
					++j;
				}
			}
			ul.append(jQuery('<li><code>' + out + '</code></li>'));
		}

		// Update result area
		result_area.empty();
		result_area.append(ul);
	}

	// Data successfully processed
	function on_result_success(result) {
		display_result(result.data);
		set_submit_button_active();
	}

	// Data processing failed
	function on_result_failure() {
		set_submit_button_active();
	}

	// Input parsing failed
	function on_parsing_failure(error) {
		alert('in input file, parsing error ' + error);
		set_submit_button_active();
	}

	// Setup
	cloud = new Cloud();
	cloud.setup();

	// Define form submission action
	$('#input-form').submit(function(event) {
		event.preventDefault();

		var request_data = { };

		// Retrieve target-size
		if (jQuery.isNumeric($('#target-size').val()))
			request_data['target-size'] = parseInt($('#target-size').val(), 10);

		// Retrieve min-freq
		if (jQuery.isNumeric($('#min-freq').val()))
			request_data['min-freq'] = parseInt($('#min-freq').val(), 10);

		// Retrieve use-equivalences
		if ($('#use-eq').is(':checked'))
			request_data['use-equivalences'] = true;
		
		// Retrieve input file
		var input_file = $('#input-file')[0].files[0];

		// Update user interface
		set_submit_button_unactive();

		// Parse data and send them to the cloud
		parse_aacid_sequence_map(input_file)
			.done(function(aacid_sequence_map) {
				request_data['sequence-map'] = aacid_sequence_map;
				cloud.run('/aakit/profile', request_data).then(on_result_success, on_result_failure);
			})
			.fail(on_parsing_failure);
	});
});
