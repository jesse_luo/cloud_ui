/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Web Client for aakit/hmmscan
 */



$(document).ready(function() {
  // Display & UI
	var submit_button = $("#submit-button");
	var result_area = $("#result-area");

	function set_submit_button_unactive() {
		submit_button.addClass("disabled");
		submit_button.html("Processing...");
	}

	function set_submit_button_active() {
		submit_button.removeClass("disabled");
		submit_button.html("Submit");
	}

	function display_result(result) {
		alert(JSON.stringify(result));
	}

	// Data successfully processed
	function on_result_success(result) {
		display_result(result.data);
		set_submit_button_active();
	}

	// Data processing failed
	function on_result_failure() {
		set_submit_button_active();
	}

	// Input parsing failed
	function on_sequence_parsing_failure(error) {
		alert('in sequence file, parsing error ' + error);
		set_submit_button_active();
	}

	function on_pattern_parsing_failure(error) {
		alert('in pattern file, parsing error ' + error);
		set_submit_button_active();
	}

	// Setup
	cloud = new Cloud();
	cloud.setup();

	// Define form submission action
	$('#input-form').submit(function(event) {
		event.preventDefault();

		// Retrieve input file
		input_file = $('#input-file')[0].files[0];

		// Retrieve pattern file
		pattern_file = $('#pattern-file')[0].files[0];

		// Update user interface
		set_submit_button_unactive();

		// Parse data and send them to the cloud
		parse_aacid_sequence_map(input_file)
			.done(function(aacid_sequence_map) {
				parse_aacid_sequence_list(pattern_file)
					.done(function(aacid_pattern_list) {
						request_data = {
							'sequence-map' : aacid_sequence_map,
							'pattern-list' : aacid_pattern_list
						};
						cloud.run('/aakit/hmmscan', request_data).then(on_result_success, on_result_failure);					
					})
					.fail(on_pattern_parsing_failure);
			})
			.fail(on_sequence_parsing_failure);
	});
});
