/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Web Client for pfkit/compile
 */



$(document).ready(function() {
  // Display & UI
	var submit_button = $("#submit-button");
	var result_area = $("#result-area");
	var select_widget = $("#transcript-table");

	function set_submit_button_unactive() {
		submit_button.addClass("disabled");
		submit_button.html("Processing...");
	}

	function set_submit_button_active() {
		submit_button.removeClass("disabled");
		submit_button.html("Submit");
	}

	function set_transcript_table_selection(transcript_table_list) {
		select_widget.empty();
		var item_count = transcript_table_list.length;	
		for(var i = 0; i < item_count; ++i) {
			var item = transcript_table_list[i];
			select_widget.append(jQuery('<option value="' + item + '">' + item + '</option>'));
		}
	}

	function display_result(result) {
    var elapsed_time = moment.duration(result['elapsed-time']);
		var selection_list = result['selection-list'];

		// Container
		var root = jQuery('<div class="callout" />');
		root.append(jQuery('<h5>Solution found <small>' + elapsed_time.humanize(true) + ' </small></h5>'));
		
		// Show solution as a table
		var table = jQuery('<table><theader><th>Codons list</th><th>Aacids</th><th>Freq.</th></theader></table>');
		root.append(table);

		var tbody = jQuery('<tbody/>');
		table.append(tbody);

		var item_count = selection_list.length;
		for (var i = 0; i < item_count; i++) {
			var tr = jQuery('<tr/>');
			for (var j = 0; j < 3; ++j)
				tr.append(jQuery('<td>' + selection_list[i][j] + '</td>'));
			tbody.append(tr);
		}

		// Update result area
		result_area.empty();
		result_area.append(root);
	}

	// Data successfully processed
	function on_result_success(result) {
		display_result(result.data);
		set_submit_button_active();
	}

	// Data processing failed
	function on_result_failure() {
		set_submit_button_active();
	}

	// Input parsing failed
	function on_parsing_failure(error) {
		alert('in input profile file, parsing error ' + error);
		set_submit_button_active();
	}

	// Setup
	cloud = new Cloud();
	cloud.setup()
		.then(cloud.get_transcript_table_list)
		.then(set_transcript_table_selection);

	// Define form submission action
	$('#input-form').submit(function(event) {
		event.preventDefault();

		var transcript_table = 'human'
		var request_data = { };
		
		// Retrieve unwanted codon_set
		var codon_array = new Array();
		var token_array = $('#unwanted-codon-set').val().split(',');
		var token_count = token_array.length;
		for (var i = 0; i < token_count; i++) {
			var item = token_array[i].trim();
			if (item.length > 0) {
				codon_array.push(item.toUpperCase());
			}
		}

		// Retrieve transcript table
		transcript_table = $('#transcript-table').val();

		if (codon_array.length > 0)
			request_data['unwanted-codon-set'] = codon_array;

		// Retrieve no-consecutive-codon-repeat
		if ($('#no-consecutive-codon-repeat').is(':checked'))
			request_data['no-consecutive-codon-repeat'] = true;

		// Retrieve max-base-repeat
		if (jQuery.isNumeric($('#max-base-repeat').val()))
			request_data['max-base-repeat'] = parseInt($('#max-base-repeat').val(), 10);

		// Retrieve max-substring-len
		if (jQuery.isNumeric($('#max-substring-len').val()))
			request_data['max-substring-len'] = parseInt($('#max-substring-len').val(), 10);

		// Retrieve max-substring-len
		if (jQuery.isNumeric($('#max-library-size').val()))
			request_data['max-library-size'] = parseInt($('#max-library-size').val(), 10);

		// Retrieve input file
		var input_file = $('#input-file')[0].files[0];

		// Update user interface
		set_submit_button_unactive();

		// Parse data and send them to the cloud
		parse_aacid_profile(input_file)
			.done(function(profile) {
				request_data['profile'] = profile;
				cloud.run('/pfkit/compile/' + transcript_table, request_data).then(on_result_success, on_result_failure);
			})
			.fail(on_parsing_failure);
	});
});
