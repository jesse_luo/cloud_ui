/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * User Interface generation utilities
 */



function ui_generate_tabs(name, root, tab_name_array, generate_tab_content) {
	var tab_name_count = tab_name_array.length;

	// Generate tabs
	var tab = jQuery('<div/>', { id : name, 'data-tabs' : null });
	tab.addClass('tabs');

	for(var i = 0; i < tab_name_count; i++) {
		var li = jQuery('<li/>');
		li.addClass('tabs-title');

		var a = document.createElement('a');
		a.href = '#panel' + i;
		a.innerHTML = tab_name_array[i];

		if (i == 0) {
			li.addClass('is-active');
			a.setAttribute('aria-selected', 'true');
		}

		li.append(a);
		tab.append(li);
	}

	// Generate tabs content
	var tab_content = jQuery('<div/>', { 'data-tabs-content' : name });
	tab_content.addClass('tabs-content');

	for(var i = 0; i < tab_name_count; i++) {
		var div = jQuery('<div/>', { id : 'panel' + i });
		div.addClass('tabs-panel');
		if (i == 0)
			div.addClass('is-active');

		div.append(generate_tab_content(tab_name_array[i]));
		tab_content.append(div);
	}

	// Attach to root
	root.append(tab);
	root.append(tab_content);
}
