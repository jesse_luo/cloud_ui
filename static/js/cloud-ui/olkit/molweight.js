/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Web Client for olkit/molweight
 */



$(document).ready(function() {
    // Display & UI
    var submit_button = $("#submit-button");
    var result_area = $("#result-area");

    function set_submit_button_unactive() {
        submit_button.addClass("disabled");
        submit_button.html("Processing...");
    }

    function set_submit_button_active() {
        submit_button.removeClass("disabled");
        submit_button.html("Submit");
    }

    function display_result(result) {
        var name_array = Array();
        for (var sequence_name in result) {
            if (result.hasOwnProperty(sequence_name))
                name_array.push(sequence_name);
        };

        result_area.empty();
        ui_generate_tabs('result-tabs', result_area, name_array, function(name) {
            var div = jQuery('<div id="chart"/>');
            return div;
        });

        //Create chart

        var tabs = new Foundation.Tabs(result_area);
        var panel_index = 0;
        for (var j = 0; j < name_array.length; j += 1) {
            var molecules = [];
            var tickNames = [];
            var result_list = result[name_array[j]];
            result_list.sort(); //sort by molecular weight
            for (i = 0; i < result_list.length; i += 1) {
                molecules.push({
                    molweight: Number(result_list[i][0]).toFixed(2),
                    probability: result_list[i][1]
                });
                tickNames.push(Number(result_list[i][0]).toFixed(2)); //0 contains the mol weight molecules Y
            };

            var width = 400;
            var height = 400;
            var padding = 80;
            var panel_selector = "div#panel" + panel_index;

            var svg = d3.select(panel_selector)
                .append("svg")
                .attr("width", width)
                .attr("height", height);

            //tip

            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .html(function(d) {
                    return "p = " + d.probability;
                })
                .direction("e");

            svg.call(tip);
            //Scales

            var xScale = d3.scale.linear()
                .domain([0, d3.max(molecules, function(d) {
                    return d.probability;
                })])
                .range([0, width - padding]);

            var yScale = d3.scale.ordinal()
                .domain(d3.range(molecules.length))
                .rangeRoundBands([padding, height - padding], 0.8);

            //Axes

            var xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(yScale)
                .orient("left")
                .tickSize(2)
                .tickFormat(function(d, i) {
                    return molecules[i].molweight;
                })
                .tickValues(d3.range(molecules.length));

            var y_xis = svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" +
                    padding + ", 0)")
                .call(yAxis);

            var x_xis = svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(" +
                    padding + ", 316)")
                .call(xAxis);

            svg.selectAll(".bar")
                .data(molecules)
                .enter()
                .append("rect")
                .attr("class", "bar")
                .attr("x", 0 + padding)
                .attr("y", function(d, i) {
                    return yScale(i);
                })
                .attr("width", function(d) {
                    return xScale(d.probability);
                })
                .attr("height", yScale.rangeBand)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

            svg.append("text")
                .attr("class", "x label")
                .attr("text-anchor", "end")
                .attr("x", width)
                .attr("y", height - 40)
                .text("probability");

            svg.append("text")
                .attr("class", "y label")
                .attr("text-anchor", "end")
                .attr("y", 6)
                .attr("x", -100)
                .attr("dy", ".75em")
                .attr("transform", "rotate(-90)")
                .text("molweight");

            panel_index += 1;
        };
    }

    // Data successfully processed
    function on_result_success(result) {
        display_result(result.data);
        set_submit_button_active();
    }

    // Data processing failed
    function on_result_failure() {
        set_submit_button_active();
    }

    // Input parsing failed
    function on_parsing_failure(error) {
        alert('in input file, parsing error ' + error);
        set_submit_button_active();
    }

    // Setup
    cloud = new Cloud();
    cloud.setup();

    // Define form submission action
    $('#input-form').submit(function(event) {
			event.preventDefault();

			var request_data = {};

			// Retrieve input file
			var input_file = $('#input-file')[0].files[0];

			// Update user interface
			set_submit_button_unactive();

			// Parse data and send them to the cloud
			parse_dna_sequence_map(input_file, true)
				.done(function(dna_sequence_map) {
					request_data['sequence-map'] = dna_sequence_map;
					cloud.run('/olkit/molweight', request_data).then(on_result_success, on_result_failure);
				})
				.fail(on_parsing_failure);
    });
});
