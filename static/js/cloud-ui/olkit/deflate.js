/*
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * Web Client for olkit/deflate
 */



$(document).ready(function() {
  // Display & UI
	var submit_button = $("#submit-button");
	var result_area = $("#result-area");

	function set_submit_button_unactive() {
		submit_button.addClass("disabled");
		submit_button.html("Processing...");
	}

	function set_submit_button_active() {
		submit_button.removeClass("disabled");
		submit_button.html("Submit");
	}

	function display_result(result) {
		var name_array = Array();
		for (var sequence_name in result)
			if (result.hasOwnProperty(sequence_name))
				name_array.push(sequence_name);
		
		result_area.empty();
		ui_generate_tabs('result-tabs', result_area, name_array, function(name) {
			var table = jQuery('<table><theader><th>Pos.</th><th>Codon</th><th>Content</th></theader></table>');

			var tbody = jQuery('<tbody/>');
			table.append(tbody);

			var item_list = result[name];
			var item_count = item_list.length;
			for (var i = 0; i < item_count; i++) {
				var tr = jQuery('<tr/>');
				for (var j = 0; j < 2; ++j)
					tr.append(jQuery('<td>' + item_list[i][j] + '</td>'));
				tr.append(jQuery('<td>' + item_list[i][2].join(' ') + '</td>'));
				tbody.append(tr);
			}

			return table;
		});

		var tabs = new Foundation.Tabs(result_area);
	}

	// Data successfully processed
	function on_result_success(result) {
		display_result(result.data);
		set_submit_button_active();
	}

	// Data processing failed
	function on_result_failure() {
		set_submit_button_active();
	}

	// Input parsing failed
	function on_parsing_failure(error) {
		alert('in input file, parsing error ' + error);
		set_submit_button_active();
	}

	// Setup
	cloud = new Cloud();
	cloud.setup();

	// Define form submission action
	$('#input-form').submit(function(event) {
		event.preventDefault();

		var request_data = { };

		// Retrieve special codon_set
		var codon_array = new Array();
		var token_array = $('#input-special-codon-set').val().split(',');
		var token_count = token_array.length;
		for (var i = 0; i < token_count; i++) {
			var item = token_array[i].trim();
			if (item.length > 0) {
				codon_array.push(item.toUpperCase());
			}
		}

		if (codon_array.length > 0)
			request_data['special-codon-set'] = codon_array;

		// Retrieve input file
		var input_file = $('#input-file')[0].files[0];

		// Update user interface
		set_submit_button_unactive();

		// Parse data and send them to the cloud
    parse_dna_sequence_map(input_file, true)
			.done(function(dna_sequence_map) {
  	    request_data['sequence-map'] = dna_sequence_map;
				cloud.run('/olkit/deflate', request_data).then(on_result_success, on_result_failure);
  	  })
			.fail(on_parsing_failure);
	});
});
