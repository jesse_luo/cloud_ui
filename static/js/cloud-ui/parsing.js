/* 
 * Jesse Luo, Alexandre Devert, Adagene Inc.
 * Copyright (c) 2016 All Rights Reserved.
 *
 * File parsing functions
 * Parsing is done server side, but the plan is to do it client side eventually.
 */

function parse_aacid_sequence_map(input_file_obj) {
	var ret = $.Deferred();

	// Setup the form data
	var form_data = new FormData();
	form_data.append('input.txt', input_file_obj)

	// Send post request
	$.ajax({
		url         : '/api/parsing/aacid-sequence-map',
		type        : 'POST',
		data        : form_data,
		cache       : false,
		processData : false, 
		contentType : false,
    enctype     : 'multipart/form-data',
		dataType    : 'json'
	})
	.done(function(data, status, jqXHR) {
		ret.resolve(data);
	})
	.fail(function(jqXHR, status, error) {
		ret.reject(jqXHR, status, error);
	});

	// Return result promise
	return ret.promise();
}

function parse_aacid_sequence_list(input_file_obj) {
	var ret = $.Deferred();

	// Setup the form data
	var form_data = new FormData();
	form_data.append('input.txt', input_file_obj)

	// Send post request
	$.ajax({
		url         : '/api/parsing/aacid-sequence-list',
		type        : 'POST',
		data        : form_data,
		cache       : false,
		processData : false, 
		contentType : false,
    enctype     : 'multipart/form-data',
		dataType    : 'json'
	})
	.done(function(data, status, jqXHR) {
		ret.resolve(data['sequence-list']);
	})
	.fail(function(jqXHR, status, error) {
		ret.reject(jqXHR, status, error);
	});

	// Return result promise
	return ret.promise();
}

function parse_dna_sequence_list(input_file_obj,
                                 degenerate) {
	var ret = $.Deferred();

	// Setup the form data
	var form_data = new FormData();
	form_data.append('input.txt', input_file_obj)
	form_data.append('degenerate', degenerate);

	// Send post request
	$.ajax({
		url         : '/api/parsing/dna-sequence-list',
		type        : 'POST',
		data        : form_data,
		cache       : false,
		processData : false, 
		contentType : false,
    enctype     : 'multipart/form-data',
		dataType    : 'json'
	})
	.done(function(data, status, jqXHR) {
		ret.resolve(data['sequence-list']);
	})
	.fail(function(jqXHR, status, error) {
		ret.reject(jqXHR, status, error);
	});

	// Return result promise
	return ret.promise();
}

function parse_dna_sequence_map(input_file_obj,
                                degenerate) {
	var ret = $.Deferred();

	// Setup the form data
	var form_data = new FormData();
	form_data.append('input.txt', input_file_obj)
	form_data.append('degenerate', degenerate);

	// Send post request
	$.ajax({
		url         : '/api/parsing/dna-sequence-map',
		type        : 'POST',
		data        : form_data,
		cache       : false,
		processData : false, 
		contentType : false,
    enctype     : 'multipart/form-data',
		dataType    : 'json'
	})
	.done(function(data, status, jqXHR) {
		ret.resolve(data);
	})
	.fail(function(jqXHR, status, error) {
		ret.reject(jqXHR, status, error);
	});

	// Return result promise
	return ret.promise();
}

function parse_aacid_profile(input_file_obj) {
	var ret = $.Deferred();

	// Setup the form data
	var form_data = new FormData();
	form_data.append('input.txt', input_file_obj)

	// Send post request
	$.ajax({
		url         : '/api/parsing/aacid-profile',
		type        : 'POST',
		data        : form_data,
		cache       : false,
		processData : false, 
		contentType : false,
    enctype     : 'multipart/form-data',
		dataType    : 'json'
	})
	.done(function(data, status, jqXHR) {
		ret.resolve(data['profile']);
	})
	.fail(function(jqXHR, status, error) {
		ret.reject(jqXHR, status, error);
	});

	// Return result promise
	return ret.promise();
}
