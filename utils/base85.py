'''
Alexandre Devert, Adagene Inc.
Copyright (c) 2012-2015 All Right Reserved,

Encoding/decoding for a string-friendly ASCII85 coding scheme
'''

code_str = '0123456789abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#'



def encode(data, padding_chr = '_'):
	out = [None] * 5

	def accumulate(acc, c):
		acc <<= 8
		acc |= ord(c)
		return acc

	def base256to85(int32):
		# Conversion to base 85
		for i in range(5):
			out[i] = int32 % 85
			int32 /= 85

		# Deliver the base 85 characters
		for b in reversed(out):
			yield code_str[b]	
	
	pos, acc = 0, 0
	for c in data:
		# Accumulate characters
		if pos < 4:
			acc = accumulate(acc, c)
		pos += 1
		
		# Encode accumulated characters
		if pos == 4:
			for b in base256to85(acc):
				yield b
			pos, acc = 0, 0

	if pos > 0:
		for i in range(pos, 4):
			acc = accumulate(acc, padding_chr)
		for b in base256to85(acc):
			yield b



def decode(data):
	pos, acc = 0, 0
	out = [None] * 4

	for c in data:
		# Accumulate characters
		if pos < 5:
			acc *= 85
			acc += code_str.index(c)
		pos += 1
		
		# Encode accumulated characters
		if pos == 5:
			pos = 0
			
			# Conversion to base 256
			for i in range(4):
				out[i] = acc & 255
				acc >>= 8

			# Deliver the base 256 characters
			for b in reversed(out):
				yield chr(b)

