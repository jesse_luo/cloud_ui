'''
Alexandre Devert, Adagene Inc.
Copyright (c) 2012-2013 All Right Reserved,

A sub-module to handle PBF (Protein Data Bank) files
'''

import os.path

from ..utils.contexts import generic_open



class HeaderHandler:
	def __init__(self):
		self.data = None

	def parse(self, line):
		self.data = dict(zip(('classification', 'date', 'id'), line.split()))

	def build(self, data):
		if self.data is not None:
			data['header'] = self.data



class AtomHandler:
	def __init__(self):
		self.atoms = { }

	def parse(self, line):
		serial       = int(line[6:11])
		name         = line[12:16].strip()
		residue_name = line[17:20].strip().capitalize()
		chain_id     = line[21]
		residue_seq  = line[22:27].strip()
		x, y, z      = (float(line[i:i+8]) for i in (30, 38, 46))
		occupancy    = float(line[54:60])
		temp_factor  = float(line[60:66])

		data = {
			'serial'       : serial,
			'name'         : name,
			'chain-id'     : chain_id,
			'residue-name' : residue_name,
			'residue-seq'  : residue_seq,
			'coords'       : (x, y, z),
			'occupancy'    : occupancy,
			'temp-factor'  : temp_factor,
		}

		self.atoms[serial] = data

	def build(self, data):
		data['atoms'] = tuple(self.atoms[i] for i in sorted(self.atoms.keys()))



def load(path_or_file, sequence_length = None):
	# Build the record handlers
	handler_map = {
		'HEADER' : HeaderHandler(),
		'ATOM  ' : AtomHandler()
	}

	# For each line
	with generic_open(path_or_file) as f:
		for line_nb, line in enumerate(f):
			record_id = line[:6]
			if record_id in handler_map:
				handler_map[record_id].parse(line)

	# Gather the data collected
	data = { }
	for handler in handler_map.values():
		handler.build(data)

	# Job done
	return data
