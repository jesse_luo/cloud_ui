'''
Alexandre Devert, Adagene Inc.
Copyright (c) 2012-2015 All Right Reserved,

A sub-module to handle BDB (Backbone Database) files, a text-file format
developed at Adagene to store backbone informations
'''

import re
import numpy

from .definitions import aacid_to_code, code_to_aacid
from .errors import ParseError


__all__ = ['load', 'Backbone', 'Section', 'Residue']



# --- Writing -----------------------------------------------------------------

def save(out, backbone):
	out.write('protein %s\n' % backbone.name)
	for section in backbone.section_list:
		prev_ca_pos = None
		out.write('section %s\n' % section.name)
		for residue in section.residue_list:
			if prev_ca_pos is None:
				ca_pos = residue.c_alpha.copy()
			else:
				ca_pos = residue.c_alpha - prev_ca_pos

			out.write('residue %s %s\n' % (residue.name, aacid_to_code[residue.aacid].upper()))
			out.write('CA %8.3f %8.3f %8.3f\n' % tuple(ca_pos))
			if residue.c_atom is not None:
				out.write(' C %8.3f %8.3f %8.3f\n' % tuple(residue.c_atom - residue.c_alpha))
			if residue.n_atom is not None:
				out.write(' N %8.3f %8.3f %8.3f\n' % tuple(residue.n_atom - residue.c_alpha))
			if residue.o_atom is not None:
				out.write(' O %8.3f %8.3f %8.3f\n' % tuple(residue.o_atom - residue.c_alpha))
			out.write('end\n')

			prev_ca_pos = residue.c_alpha.copy()
		out.write('end\n')
	out.write('end\n')



def save_as_PDB(out, backbone):
	record      = 'ATOM'
	occupancy   = 1.
	temp_factor = 0.
	format_str  = '%-6s%5d %4s%1s%3s %1s%4s    %8.3f%8.3f%8.3f%6.2f%6.2f\n'

	i = 1
	for section in backbone.section_list:
		for residue in section.residue_list:
			aacid = code_to_aacid[residue.aacid].upper()
			atom_list = [
				('CA', residue.c_alpha),
				('C',  residue.c_atom),
				('N',  residue.n_atom),
				('O',  residue.o_atom),
			]
			for atom_name, pos in atom_list:
				out.write(format_str % (record, i, atom_name, ' ', aacid, section.name, residue.name, pos[0], pos[1], pos[2], occupancy, temp_factor))
				i += 1



# --- Parsing -----------------------------------------------------------------

def enumerate_char_from_file(in_file, buffer_size = 4096):
	while True:
		chunk = in_file.read(buffer_size)
		for c in chunk:
			yield c

		if len(chunk) < buffer_size:
			return



def tokenize(in_file):
	line_id = 1
	token = None
	for c in enumerate_char_from_file(in_file):
		if c == '\n':
			line_id += 1

		if c.isspace():
			if token is not None:
				yield line_id, ''.join(token)
				token = None
		else:
			if token is None:
				token = [c]
			else:
				token.append(c)

	if token is not None:
		yield line_id, ''.join(token)



keyword_set = frozenset(['protein', 'section', 'residue', 'end', 'C', 'CA', 'N', 'O', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y'])



def enumerate_lexeme(in_file):
	float_point_re = re.compile(r'[-+]?[0-9]*\.?[0-9]+')

	for line_id, token in tokenize(in_file):
		float_point_match = float_point_re.match(token)
		if token in keyword_set:
			yield line_id, token, token
		elif float_point_match and len(float_point_match.group(0)) == len(token):
			yield line_id, token, float(token)
		else:
			yield line_id, token, token



class Parser(object):
	def __init__(self, lexer):
		self.lexer = lexer
		self.symbol = None
		self.line_id = None

	def next(self):
		line_id, token, value = self.lexer.next()
		self.line_id = line_id
		self.lookahead = (token, value)

	def accept(self, symbol = None):
		if symbol in keyword_set:
			return self.lookahead[0] == symbol
		elif symbol is str:
			return True
		elif symbol is float:
			return isinstance(self.lookahead[1], float)

		return True

	def consume(self, symbol = None):
		if not self.accept(symbol):
			raise ParseError(self.line_id, 'unexpected symbol "%s", excepted "%s"' % (self.lookahead[0], symbol))

	def consume_from_set(self, symbol_set):
		for symbol in symbol_set:
			if self.accept(symbol):
				return symbol

		raise ParseError(self.line_id, 'unexpected symbol "%s", excepted %s' % (self.lookahead[0], ', '.join('"%s"' % c for c in symbol_set)))

	def parse_vector(self):
		ret = numpy.zeros(3)
		for i in range(3):
			self.next()
			self.consume(float)
			ret[i] = self.lookahead[1]
		return ret

	def parse_residue(self):
		self.consume('residue')

		self.next() # name
		self.consume(str)
		name = self.lookahead[0]

		self.next() # aacid
		self.consume()
		aacid = self.lookahead[1].lower()

		residue = Residue(name)
		residue.aacid = aacid

		atom_set = set(['CA', 'C', 'N', 'O'])
		while len(atom_set) > 0:
			self.next()
			symbol = self.consume_from_set(atom_set)
			atom_set.discard(symbol)
			if symbol == 'CA':
				residue.c_alpha = self.parse_vector()
			elif symbol == 'C':
				residue.c_atom = self.parse_vector()
			elif symbol == 'N':
				residue.n_atom = self.parse_vector()
			elif symbol == 'O':
				residue.o_atom = self.parse_vector()			

		self.next()
		self.consume('end')

		return residue

	def parse_residue_list(self):
		while True:
			self.next()
			if self.accept('residue'):
				yield self.parse_residue()
			else:
				self.consume('end')
				break

	def parse_section(self):
		self.consume('section')

		self.next() # name
		self.consume(str)
		name = self.lookahead[0]

		section = Section(name)
		section.residue_list.extend(self.parse_residue_list())
		return section

	def parse_section_list(self):
		while True:
			self.next()
			if self.accept('section'):
				yield self.parse_section()
			else:
				self.consume('end')
				break		

	def parse_protein(self):
		self.consume('protein')

		self.next() # name
		self.consume(str)
		name = self.lookahead[0]

		backbone = Backbone(name)
		backbone.section_list.extend(self.parse_section_list())
		return backbone

	def parse_protein_list(self):
		while True:
			try:
				self.next()
			except StopIteration:
				break

			if self.accept('protein'):
				yield self.parse_protein()
			else:
				raise ParseError(self.line_id, 'Unexpected symbol "%s", instead of end of file' % self.lookahead[0])



def kahan_sum(input_seq):
	X = input_seq.next()
	S = numpy.zeros(X.shape)
	C = numpy.zeros(X.shape)
	while True:
		try:
			Y = X - C
			T = S + Y
			C = (T - S) - Y
			S = T
			yield S.copy()
			X = input_seq.next()
		except StopIteration:
			break



def load(in_file):
	parser = Parser(enumerate_lexeme(in_file))

	# For each parsed backbone
	for backbone in parser.parse_protein_list():
		for section in backbone.section_list:
			# Put calpha in absolute frame
			S = (residue.c_alpha for residue in section.residue_list)
			for residue, X in zip(section.residue_list, kahan_sum(S)):
				residue.c_alpha = X

			# Put all atoms coordinates in absolute frame
			for residue in section.residue_list:
				residue.c_atom += residue.c_alpha
				residue.n_atom += residue.c_alpha
				residue.o_atom += residue.c_alpha

		# Job done
		yield backbone



# --- Container objects -------------------------------------------------------

class Residue(object):
	def __init__(self, name):
		self.name = name
		self.c_alpha = None
		self.c_atom = None
		self.n_atom = None
		self.o_atom = None



class Section(object):
	def __init__(self, name):
		self.name = name
		self.residue_list = []



class Backbone(object):
	def __init__(self, name):
		self.name = name
		self.section_list = []
