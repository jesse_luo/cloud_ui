Adagene Webservice
====================
1. [Introduction](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-introduction)
2. [Implementation](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-implementation)
3. [Usage](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-usage)
4. [Code Examples](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-code-examples)
5. [Example Inputs](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-example-inputs)
6. [Starting Servers](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-starting-servers)
7. [Notes](https://bitbucket.org/jesse_luo/ada_web/overview#notes)
8. [Future Plans](https://bitbucket.org/jesse_luo/ada_web/overview#markdown-header-future-plans)

- - -

## Introduction

This is a web client created for the purpose of interacting with the web services developed for Alexander Devert's algorithms for Adagene Inc. Those web services use an XML-RPC call to retrieve information from a server, and this web client is intended to make this process easier to use by abstracting away the command line interface and using a GUI instead. It was created by Jesse Luo during winter break between 2015-2016 and is his first project.

Jesse Luo is a 2nd year computer science at UC Berkeley.

## Implementation

The front end was created using the Foundation framework and basic html. No styling has been done
other than the default elements provided by the framework. A lot of templating was done since the format is repeated throughout the web client. Data is sent to the backend by using Javascript and the jQuery library. The backend is implemented using Flask and Python 2.7. The backend is responsible for cleaning up the data that is provided and sending it to the server via XML-RPC in the proper JSON dictionary format.

## Usage

The web client intends to exist for every single service that is available. This would be four different groups:

1. aakit
	* hmmscan
	* profile
2. olkit
	* molweight
	* deflate
3. pfkit
	* profile compilation
4. coin
	* clustering
	* prediction

Each one of these methods has the following commands: Submit, Status, Fetch, Delete.

#### Submit

Submit requires a path to the file/directory that you are submitting. The text 
in the box will specify what it's looking for. The two ways to write the paths 
to the file/files are as follows.

*Directory*

`Users/jesseluo/Desktop/files/`

*File*

`Users/jesseluo/Desktop/files/test.fasta`

Once the file path has been written, press submit to submit the file to the 
server, if successful, it will return a task name. Otherwise, most likely it 
has errored and right now the debugging isn't great, so it will not specify 
what the error was, or at least not very accurately. Save this task name 
somewhere because it will be necessary for any status checking, result 
fetching, and process deleting.

#### Status

Status requires a task name for the task you are wishing to check the status 
on. It will report the status right below the button whether its *completed*, 
*processing*, or *waiting*. Nothing too sophisticated is required for this 
function, just type in the task name and press status.

#### Results

Result requires a task name for the task you wish to see the results for, and 
then it will output the results in a JSON dictionary format into a 
**results.txt** file. This file will automatically be downloaded and inside 
will be the results.

#### Delete

Delete requires a task name for the task you wish to delete. Tasks can *only* 
be deleted when they are *not* being processed (in queue or completed). It's 
recommended to delete tasks once they are completed and the results are no 
longer needed to free up memory for the servers.

## Code Examples

**Example 1.** Routing.

```
#!python
@app.route("/page/status", methods=['POST'])
def status():
	task_name = request.form['task']
	try:
		status = method.status(task_name)
		return status
	except Exception, e:
		return task_name + " was not found." 		
```

The webpage submits a POST request after pressing the button which will send 
information that can be accessed via request.form and then that information is 
used to make the XML-RPC call on the respective server.

**Example 2:** Connection to server.

```
#!python
try:
  server = xmlrpclib.ServerProxy(server_url)
  status = server.get_task_status(name)
except socket.error as msg:
  sys.stderr.write('%s\n' % msg)
except xmlrpclib.Fault as err:
  sys.stderr.write('fault %d: %s\n' % (err.faultCode, err.faultString))
except xmlrpclib.ProtocolError as err:
  sys.stderr.write('protocol error %d: %s\n' % (err.errcode, err.errmsg))
  sys.stderr.write('  URL: %s\n' % err.url)
```

This is an example of using the xmlrpclib to connect to the server and make the 
XML-RPC call. The server_url will be provided by the user in the interface 
(a host address and port number) and the connection will be initiated. For each 
server, *get_task_status* will return the status of the task which will then be
 displayed in the front end. The correct server must be started in order for 
this connection to succeed.

**Example 3.** JSON dictionary creation.

```
#!python
return { 'sequences' : { name : dna_str for name, dna_str in zip(name_list, dna_str_list) } }
```

This is an example of the formatting for a dictionary that will be provided to 
the server as submission. The json library is used to turn this dictionary into 
a JSON format, specifically json.dumps. This shows how each service requires 
data cleaning in order for the submission to succeed or an error will occur.

## Example Inputs

### COIN

##### Clustering
No PDB Files are provided here, but all you have to do is submit an absolute 
path to the directory to the web client with all the PDB files inside.

##### Prediction
Algorithm currently broken, but everything is in place for it to be implemented.

### OLKIT

##### MolWeight
This one requires a file with sequence names and their sequences after them 
with a space in between. You are welcome to use any extension in your file. So 
for example, make a file called test.seq and then put the following into it

```
seq-3141 TGCGCCAGGRGCRSCRSCRHAGRGTTCGMCTACTGGGGC
seq-9112 TGCGCCAGGRGCRSCRSCRHAGYMCTCGMCTACTGGGGC
```

This should return you a valid result that the system is working. If you have 
more sequences, just follow the format of the above.

##### Deflate
Same input format as above.

### AAKIT

##### HMMscan
This one requires two files, one with the sequence names and their sequences, 
the other one requires a pattern. So you have two files, test.seq and 
pattern.seq. Examples are shown below.
```
#test.seq
seq-3141 TGCGCCAGGRGCRSCRSCRHAGRGTTCGMCTACTGGGGC
seq-9112 TGCGCCAGGRGCRSCRSCRHAGYMCTCGMCTACTGGGGC
```
```
#pattern.seq
lol-1 carggglgfdywg
lol-2 carsgglgfdywg
lol-3 cargtglgfaywg    
lol-4 carsggigfdywg
```
Put these each in a file and submit it to the web service via their paths.

##### Profile
This one is similar to above. It is generating a profile so it needs amino acid 
sequences.
```
#test.seq
seq-3141 TGCGCCAGGRGCRSCRSCRHAGRGTTCGMCTACTGGGGC
seq-9112 TGCGCCAGGRGCRSCRSCRHAGYMCTCGMCTACTGGGGC
```
All the amino acid sequences have to be the same length. But submit in this format.
There are only 3 options, and the inputs are as follows:


### Optional Inputs and Usage

| option                     | input | description|                                                                                                                                                                                                                            
|-----------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| min-freq         | integer | entry is a string value : either an integer value(frequency in amino-acid count), or,a floating point followed by the '%' character (frequency in per-cents). Amino-acids which,occurrence frequency under the specified value will not appear in the profile. |
| use-equivalences | boolean | entry is a boolean, indicating if the profile can be optimized using amino-,acid equivalences substitutions rules.                                                                                                                                             |
| target-size      | integer | entry specify a maximum size for the result profile, that which will be reached,by removing the least frequent amino-acid. It's mutually exclusive with min-freq.                                                                                              |

### PFKit

PFKit has a lot of options which I will not document here, they are documented 
in the wiki. Read the input boxes for descriptions.
For inputs, you will require a path to a profile file. An example one is below:
```
12345678901234567890
crssqslldsddgntyldwy
--------------------
.........ie.ay......
       f ry e       
         t          
--------------------
Library size: 24
```
Furthermore, when the server is started, it will have a transcript tables 
directory, and in that directory will be various transcript tables that can be 
used. One has to be provided, so provide the full name of the file in that 
input box:
```
transcript-tables.txt
```
Would be an example. All test files will be uploaded onto the repository for usage.
These are the two inputs minimum required for usage.

### Optional Inputs and Usage

| option                     | input | description                                                                                                                                                                                                                            
|-----------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| timeout                     | integer | maximum time allowed to process the task, in seconds|
| restriction-list            | path    | DNA restriction to apply. name is the name of the restriction, start and end is the start and end (inclusive) position of the restriction in the profile, left_sided specify a preference to left or right side of the interval. [ ["AlwXI", [[0, 8, false], [18, 26, true]]] ] |
| unwanted-dna-str            | path    | DNA strings that should be suppressed everywhere in the DNA library. The DNA strings are specified as restriction names                                                                                                                                                         |
| unwanted-aacid-str          | path    | amino-acid strings that should not be produced by the DNA library, if not already present in the input profile. Override the list used by default by the server                                                                                                                 |
| unwanted-codon-set          | path    | codons that are not wanted, separate by spaces                                                                                                                                                                                                                                  |
| no-consecutive-codon-repeat | boolean | type yes if no consecutive, no if otherwise                                                                                                                                                                                                                                     |
| max-base-repeat             | integer | maximum number of consecutive occurrence of the same DNA base in the output DNA library. If absent, this constraint is not enforced.                                                                                                                                            |
| max-substring-len           | integer | maximum length of DNA substrings that can appear more than once in the output DNA library. If absent, this constraint is not enforced.                                                                                                                                          |

## Starting Servers
Here are some commands to start the servers locally once all the packages are installed.
```
#pfkit, transcript tables provided in test_files directory
pfkit compile-server --transcript-dir=PATH_TO_TRANSCRIPT_TABLES/transcript-tables --host=0.0.0.0 --port=9000

#coin
coin cluster-server --host=0.0.0.0 --port=9000

#aakit
aakit profile-server --host=0.0.0.0 --port=9000
aakit hmmscan-server --host=0.0.0.0 --port=9000

#olkit
olkit molweight-server --host=0.0.0.0 --port=9000
olkit deflate-server --host=0.0.0.0 --port=9000
```

## Notes

* This web app was created over the span of a couple weeks.
* A virtual env was used to maintain packages and such makes it much easier since it runs on Python2.7.
* It retains a very simple interface that is not really looking to do anything sophisticated, but instead trying to accomplish the function of being accessible to scientists.
* Each service has its own functions for handling the input given and putting it into the proper format for the server.
* Most of the data cleaning/adjusting tools were created by Alex and slightly modified to be adapted to usage in this web service.
* Servers must be started separately and be running when this is used, otherwise nothing will occur.
* The system is modular so that parts can be added easily.
* Some basic error messages are thrown right now, but those need to be improved and more need to be created to accommodate the large amount of errors that can occur.

## Future Plans

* Expand the interface and make it more user friendly and more pretty.
* Clean up code and optimize it where I can.
* Finish writing lots of error messages.
* Handle errors as smoothly as possible and attempt to give as much user feedback as possible.
* Implement more elegant Javascript instead of using jQuery.
* Make data visualizations of certain results.
* Create login system to allow for higher security and also record access/logins.
* Change from foundation to material design.
* Standardize input names to allow for more clarity.
* Expand documentation for easier usage.
